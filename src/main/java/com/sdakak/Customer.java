package com.sdakak;

import com.google.gson.annotations.SerializedName;
import java.util.Date;
import java.util.Objects;

/*
  Simple Data class to hold each deal
 */
public class Customer implements Comparable<Customer> {

  private String name;
  @SerializedName("close_date")
  private Date closeDate;
  @SerializedName("referred_by")
  private String referrer;

  public Customer(String name, Date closeDate, String referrer) {
    this.name = name;
    this.closeDate = closeDate;
    this.referrer = referrer;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getCloseDate() {
    return closeDate;
  }

  public void setCloseDate(Date closeDate) {
    this.closeDate = closeDate;
  }

  public String getReferrer() {
    return referrer;
  }

  public void setReferrer(String referrer) {
    this.referrer = referrer;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Customer customer = (Customer) o;
    return Objects.equals(name, customer.name);
  }

  @Override
  public int hashCode() {

    return Objects.hash(name);
  }

  @Override
  public String toString() {
    return "Customer{" +
        "name='" + name + '\'' +
        ", closeDate=" + closeDate +
        ", referrer='" + referrer + '\'' +
        '}';
  }

  @Override
  public int compareTo(Customer o) {
    if (getCloseDate().compareTo(o.getCloseDate()) > 0) {
      return 1;
    } else if (getCloseDate().compareTo(o.getCloseDate()) < 0) {
      return -1;
    } else {
      return 0;
    }
  }
}

