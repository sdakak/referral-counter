package com.sdakak;

import com.google.common.annotations.VisibleForTesting;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
  Read json files containing deals and print monthly referral count for each referring customer
 */
public class OriginalReferrers {

  private String filename = "src/main/resources/customers.txt";
  private static Gson gson = new GsonBuilder().setDateFormat("MM-dd-yyyy").create();
  private Map<Date, Map<Customer, Integer>> monthlyReferrals = null;

  public OriginalReferrers() {}

  public OriginalReferrers(String filename) {
    this.filename = filename;
  }

  // control method that calls the functions that do the actual work
  public void calculateReferralsByMonth() {
    Map<String, Customer> customerMap = readCustomersFromFile();
    Map<Customer, Customer> originalReferrers = calculateOriginalReferrers(customerMap);
    monthlyReferrals = calculateMonthlyReferrals(originalReferrers);
    printMonthlyReferrals(monthlyReferrals);
  }

  // read json records from file and put in Map for fast future access
  private Map<String, Customer> readCustomersFromFile() {
    List<Customer> customerList = null;
    Map<String, Customer> customerMap = new HashMap<>();

    // Read Json
    try (Reader reader = new FileReader(filename)) {
      customerList = gson.fromJson(reader, new TypeToken<List<Customer>>() {
      }.getType());
    } catch (IOException e) {
      e.printStackTrace();
    }

    Collections.sort(customerList);

    customerList.forEach(customer -> {
      customerMap.putIfAbsent(customer.getName(), customer);
    });

    return customerMap;
  }

  // calculate a Map of Customer -> RootReferrer
  private Map<Customer, Customer> calculateOriginalReferrers(Map<String, Customer> customerMap) {
    HashMap<Customer, Customer> originalReferrers = new HashMap<>();

    // construct Map of Customer -> DirectReferrer
    for (String name : customerMap.keySet()) {
      Customer customer = customerMap.get(name);
      if (customer.getReferrer() != null) {
        originalReferrers.putIfAbsent(customer, customerMap.get(customer.getReferrer()));
      }
    }

    // Transform Map to Customer -> RootReferrer by traversing down till we hit no referrers
    for (Customer customer : originalReferrers.keySet()) {
      Customer referrer = originalReferrers.get(customer);
      Customer originalReferrer = referrer;
      while (referrer != null) {
        originalReferrer = referrer;
        referrer = originalReferrers.get(referrer);
      }
      originalReferrers.put(customer, originalReferrer);
    }

    return originalReferrers;
  }

  // Aggregate referrals by month in a new map
  private Map<Date, Map<Customer, Integer>> calculateMonthlyReferrals(Map<Customer, Customer> originalReferrers) {
    Map<Date, Map<Customer, Integer>> monthlyReferrals = new HashMap<>();

    for (Customer customer : originalReferrers.keySet()) {
      Customer referrer = originalReferrers.get(customer);
      if (referrer != null) {
        // Set day of the date to 1 since we want to aggregate by month/year and day is immaterial
        Calendar cal = Calendar.getInstance();
        cal.setTime(customer.getCloseDate());
        cal.set(Calendar.DATE, 1);
        Date month = cal.getTime();

        monthlyReferrals.putIfAbsent(month, new HashMap<>());
        Map<Customer, Integer> referralCount = monthlyReferrals.get(month);
        referralCount.putIfAbsent(referrer, 0);
        // credit 1 referral each time
        referralCount.compute(referrer, (key, value) -> value += 1);
      }
    }
    return monthlyReferrals;
  }

  // helper method to pretty print
  private void printMonthlyReferrals(Map<Date,Map<Customer,Integer>> monthlyReferrals) {
    Set<Date> months = monthlyReferrals.keySet();
    List<Date> sortedMonths = new ArrayList<Date>(months);
    Collections.sort(sortedMonths);

    for (Date month : sortedMonths) {
      Calendar cal = Calendar.getInstance();
      cal.setTime(month);
      System.out.println(cal.get(Calendar.MONTH)+1 + "/" + cal.get(Calendar.YEAR));
      for (Customer customer : monthlyReferrals.get(month).keySet()) {
        System.out.println(customer.getName() + ": " + monthlyReferrals.get(month).get(customer));
      }
      System.out.println();
    }
  }

  @VisibleForTesting
  public Map<Date, Map<Customer, Integer>> getMonthlyReferrals() {
    return monthlyReferrals;
  }
}
