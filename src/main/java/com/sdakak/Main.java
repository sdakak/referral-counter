package com.sdakak;

/*
  Main class that exercises the InitialReferrer
 */
public class Main {

  public static void main(String args[]) {
    OriginalReferrers initialReferrers = new OriginalReferrers();
    initialReferrers.calculateReferralsByMonth();
  }
}
