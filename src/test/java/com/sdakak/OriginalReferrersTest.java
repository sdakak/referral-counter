package com.sdakak;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import junit.framework.TestCase;
import org.junit.Test;

public class OriginalReferrersTest extends TestCase {


  @Test
  public void testThreeLevelDeepReferrers() throws ParseException {
    System.out.println("Three level deep referrers test");
    OriginalReferrers originalReferrers = new OriginalReferrers("src/test/resources/customers1.txt");
    originalReferrers.calculateReferralsByMonth();
    Map<Date, Map<Customer, Integer>> monthlyReferrals = originalReferrers.getMonthlyReferrals();
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    Date date = sdf.parse("01/01/2018");
    assertEquals(3, (int) monthlyReferrals.get(date).get(new Customer("A", date, "B")));
  }

  @Test
  public void testTwoLevelDeepReferrers() throws ParseException {
    System.out.println("Two level deep referrers test");
    OriginalReferrers originalReferrers = new OriginalReferrers("src/test/resources/customers2.txt");
    originalReferrers.calculateReferralsByMonth();
    Map<Date, Map<Customer, Integer>> monthlyReferrals = originalReferrers.getMonthlyReferrals();
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    Date date = sdf.parse("01/01/2018");
    assertEquals(2, (int) monthlyReferrals.get(date).get(new Customer("Dunder Mifflin", date, "B")));
  }

}
