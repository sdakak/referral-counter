How to run
==========
- Make sure java and maven is installed correctly on your machine
- Clone the repository and cd into it
- mvn clean install
- java -jar target/referral-counter-1.0-SNAPSHOT.jar

It should look like this:
```
sdaka@DESKTOP-3LCA93G MINGW64 ~/Dropbox/active/code/referral-counter (master)
$ mvn clean install
[INFO] Scanning for projects...
[INFO]
[INFO] --------------------< com.sdakak:referral-counter >---------------------
[INFO] Building Java Quickstart Project Skeleton 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ referral-counter ---
[INFO] Deleting C:\Users\sdaka\Dropbox\active\code\referral-counter\target
[INFO]
[INFO] --- maven-enforcer-plugin:1.0:enforce (enforce-maven) @ referral-counter ---
[INFO]
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ referral-counter ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource

...

[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 2.799 s
[INFO] Finished at: 2018-06-13T22:52:42-07:00
[INFO] ------------------------------------------------------------------------

sdaka@DESKTOP-3LCA93G MINGW64 ~/Dropbox/active/code/referral-counter (master)
$ java -jar target/referral-counter-1.0-SNAPSHOT.jar
1/2018
Initech: 1
Dunder Mifflin: 2

2/2018
Initech: 1
```

Tests
=====
There are two tests included which test if referral attribution works with two and three level nesting.

Here's output from a sample run:

![test-output-screenshot](src/test/resources/test-results.PNG "test-output")

Algorithm
=========
- Read customers from json file into map (String customerName -> Customer) for fast future access
- Make a map with direct referrals (Customer -> Referring Customer)
- Transform this map to get Root Referrer by traversing until you hit null (Customer -> Root Referring Customer)
- Aggregate these by month and count the referrals for each referring customer
- Print it in desired format